<?php

namespace App\Controller\api\v1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController ]
class TestController extends AbstractController
{
    #[Route(path: "/api/v1/test")]
    public function test()
    {
        return $this->json('ok');
    }
}
